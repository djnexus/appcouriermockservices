const setMock = require("../modules/globalVariables");

describe("Production environment", () => {
    it("should have the setMock variable set to false", () => {
        expect(setMock).toBe(false);
    });
});