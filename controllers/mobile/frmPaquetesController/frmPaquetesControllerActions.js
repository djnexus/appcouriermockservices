define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnPackage **/
    AS_Button_j783af0f2db544a88442e3ded3385b5a: function AS_Button_j783af0f2db544a88442e3ded3385b5a(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmPaquetes");
        ntf.navigate();
    },
    /** onClick defined for btnOfficinas **/
    AS_Button_c2ba5d168e1d459981a189bb25d3fdfd: function AS_Button_c2ba5d168e1d459981a189bb25d3fdfd(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmOficinas");
        ntf.navigate();
    },
    /** onClick defined for btnPreferencias **/
    AS_Button_d3bd23bd8a134ee8b12589b0457f7e65: function AS_Button_d3bd23bd8a134ee8b12589b0457f7e65(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmPreferencias");
        ntf.navigate();
    }
});