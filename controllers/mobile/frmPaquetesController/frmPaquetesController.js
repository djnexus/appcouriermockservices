serviceName = "CourierAppServices";
integrationObj = KNYMobileFabric.getIntegrationService(serviceName);

define({ 
  onNavigate: function(){
    this.pauseNavigation();
    kony.application.showLoadingScreen("frmLoad", "Cargando...", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, false);
    this.packageData();
    this.view.segPackages.onRowClick = this.goToPackageDetails;
  },
  operationSuccess:function(res){
    this.identifyMock(res.isMock);
    packageData = res.responseObject;
    this.view.segPackages.widgetDataMap = {lblPackageName: "description", lblPackageLastStatusDate: "date", lblPackageStatus: "status", imgStatus:"img", lblHiddenInternalTracking:"trackingNumber",
                                           lblHiddenPriceToPay:"priceToPay", lblHiddenWeight: "weight", lblHiddenSupplier:"supplier", lblHiddenCourierTracking:"courierTracking", lblHiddenCourier:"courier"};
    var packageInfo = res.responseObject;
    var array = [];
    packageInfo.forEach(function(element){
      imgIconStatus = element.statusHistory[element.statusHistory.length -1].description;
      if(imgIconStatus == "Recibido miami"){
        img = "box.png";
      }else if(imgIconStatus == "Embarcado"){
        img = "avion.png";
      }else if(imgIconStatus == "Disponible para retirar"){
        img = "green_check.png";
      }else if(imgIconStatus == "Retenido por aduanas"){
        img = "icono_warning_bps.png";
      }
      var obj = {"description":element.description, "date":element.statusHistory[element.statusHistory.length -1].date, "status":imgIconStatus, "img":img, "trackingNumber":element.internalTracking,
                 "priceToPay":element.priceToPay, "weight":element.weight, "supplier":element.supplier, "courierTracking":element.courierTracking, "courier":element.courier};
      array.push(obj);
    });
    this.view.segPackages.setData(array);
    this.resumeNavigation();
    kony.application.dismissLoadingScreen();
  },
  operationFailure: function(res){
    alert("Nelson");
    this.resumeNavigation();
    kony.application.dismissLoadingScreen();
  },
  packageData: function(){
    var operationName ="getPackages";
    var data= {"packages": ""};
    var headers= {"User": "", "X-Kony-Stub-Request":setMock};
    integrationObj.invokeOperation(operationName, headers, data, this.operationSuccess, this.operationFailure);
    
  },
  goToPackageDetails:function(){
    var goToDetails = new kony.mvc.Navigation("frmPaquetesDetails");
    goToDetails.navigate(this.view.segPackages.selectedRowItems[0]);
  },
  identifyMock: function(mockStatus){
    if(mockStatus){
      return alert("Data Coming From Mock");
    }else{
      return alert("Data Coming From Services");
    }
  }


});