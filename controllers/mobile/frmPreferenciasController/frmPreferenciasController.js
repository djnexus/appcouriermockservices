define({ 
  onNavigate: function(){
    this.pauseNavigation();
    kony.application.showLoadingScreen("frmLoad", "Cargando...", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, false);
    this.view.btnLogOut.onClick = this.logOut;
    this.setDataFromGlbVar();
    this.timeOutToLogin();
  }, 
  logOut:function(){
    var logOutToLogin = new kony.mvc.Navigation("frmLogin");
    logOutToLogin.navigate();
  },
  setDataFromGlbVar: function(){
    this.view.lblFullName.text = userData.fullName;
    this.view.lblAccountNumber.text = userData.accountNumber;
    this.resumeNavigation();
    kony.application.dismissLoadingScreen();
  },
  timeOutToLogin: function(){
    kony.application.registerForIdleTimeout(1, this.logOut);
  }
});