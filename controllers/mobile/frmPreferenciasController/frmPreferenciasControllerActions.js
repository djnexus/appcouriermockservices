define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnPackage **/
    AS_Button_j4cf1b64c70c4c41b87e527b2d1d1efd: function AS_Button_j4cf1b64c70c4c41b87e527b2d1d1efd(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmPaquetes");
        ntf.navigate();
    },
    /** onClick defined for btnOfficinas **/
    AS_Button_da8d8734a57f432fb73d92b8ea0d4099: function AS_Button_da8d8734a57f432fb73d92b8ea0d4099(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmOficinas");
        ntf.navigate();
    },
    /** onClick defined for btnPreferencias **/
    AS_Button_e5a8fd2a9c7d4cd590ff6a0a8e95cced: function AS_Button_e5a8fd2a9c7d4cd590ff6a0a8e95cced(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmPreferencias");
        ntf.navigate();
    }
});