var serviceName = "CourierAppServices";
var integrationObj = KNYMobileFabric.getIntegrationService(serviceName);
define({ 
  onNavigate: function(){
    this.view.btnLogin.onClick = this.loginAuth;
  },
  loginAuth: function(){
    var operationName =  "Login";
    var username = this.view.tbUser.text;
    if(username){
      username = username.trim();
    }
    var password = this.view.tbPassword.text;
    var data= {"user": username, "password": password};
    var headers= {"X-Kony-Stub-Request":setMock};
    integrationObj.invokeOperation(operationName, headers, data, this.operationSuccess, this.operationFailure);
  }, 
  operationSuccess:function(res){ 
    this.identifyMock(res.isMock);
    userData = res.responseObject;
    this.view.Times.isVisible = false;
    if(this.view.tbUser.text){
      if(res.success === true){
        this.view.Check.isVisible = true;
        var loginSuccessfully = new kony.mvc.Navigation("frmPaquetes");
        loginSuccessfully.navigate();
        this.view.tbUser.text = "";
        this.view.tbPassword.text = "";
        this.view.Check.isVisible = false;
        this.view.Times.isVisible = false;
      }else{
        alert("Please, check your credentials");
        this.view.Check.isVisible = false;
        this.view.Times.isVisible = true;
      }
    }else{
      alert("Fields should not be empty");
      this.view.Check.isVisible = false;
      this.view.Times.isVisible = true;
    }
  },
  operationFailure:function(res){
    alert("Error while logging in, try again!");
  },
  
  identifyMock: function(mockStatus){
    if(mockStatus){
      return alert("Data Coming From Mock");
    }else{
      return alert("Data Coming From Services");
    }
  }
});

