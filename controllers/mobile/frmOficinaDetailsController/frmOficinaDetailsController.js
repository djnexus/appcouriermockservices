define({
  //Set data into its fields for each office
  onNavigate:function(context){
    this.pauseNavigation();
    kony.application.showLoadingScreen("frmLoad", "Cargando...", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, false);
    this.view.lblOficinaName.text = context.name;
    this.view.lblAddressValue.text = context.address;
    this.view.lblEmailValue.text = context.email;
    this.view.lblPhoneNumberValue.text = context.phoneNumber;
    this.view.lblScheduleValue.text = context.workingHours;  
    //open dialer
    this.view.lblPhoneNumberValue.onTouchStart = ()=>{
      kony.phone.dial(context.phoneNumber);
    };
    //setting data map for each office
    var locationOfficeData = [{lat:context.latitude, lon:context.longitude, image:"pinb.png",meta:{color:"red", label:"A"}}];
    this.view.MapOffice.locationData = locationOfficeData;
    this.resumeNavigation();
    kony.application.dismissLoadingScreen();
  }
});