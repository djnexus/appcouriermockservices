var serviceName = "CourierAppServices";
var integrationObj = KNYMobileFabric.getIntegrationService(serviceName);

define({ 
  onNavigate: function(context){
    this.pauseNavigation();
    kony.application.showLoadingScreen("frmLoad", "Cargando...", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, false);
    this.view.lblProductTitle.text = context.description;
    this.view.lblPeso.text = "Peso: "+context.weight+" Libra(s)";
    this.view.lblPrecioRetirar.text = "Precio retirar: DOP "+ context.priceToPay;
    this.view.lblTrackingInterno.text = "TrackingInterno: "+context.trackingNumber;
    this.view.lblSuplidor.text = "Suplidor: "+context.supplier;
    this.view.lblTransportista.text = "Transportista: "+context.courier;
    this.view.lblTrackingTransportista.text = "Tracking transportista: "+context.courierTracking;
    var operationName =  "getPackagesCurrentStatus";
    var data= {"trackingNumber": context.trackingNumber};
    var headers= {};
    integrationObj.invokeOperation(operationName, headers, data, this.operationSuccess, this.operationFailure);
  },
  operationSuccess:function(res){
    this.view.segPackageStatus.widgetDataMap ={
      lblDate: "date",
      lblStatus: "description",
      imgStatusImage: "img"
    };
    var detailsInfo = res.responseObject;
    var datosLimpios = [];
    detailsInfo.forEach(function(element){
      var img = {};
      if(element.description == "Recibido miami"){
        img = "box.png";
      }else if(element.description == "Embarcado"){
        img = "avion.png";
      }else if(element.description == "Disponible para retirar"){
        img = "green_check.png";
      }else if(element.description == "Retenido por aduanas"){
        img = "icono_warning_bps.png";
      }else{
        img = "icono_almacen_bps.png";
      }
      var obj = {"date":element.date, "description":"   "+element.description, "img":img};
      datosLimpios.push(obj);
      datosLimpios.reverse();
    });
    this.view.segPackageStatus.setData(datosLimpios);
    this.resumeNavigation();
    kony.application.dismissLoadingScreen();
  },
  operationFailure:function(res){
    alert("Fatal Error!");
    this.resumeNavigation();
  },
  goBack: function(){
    var ntf = new kony.mvc.Navigation(kony.application.getPreviousForm().id);
  }
});