define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onRowClick defined for segOficinasList **/
    AS_Segment_a1f2060eec6a49a58f0de2fe8116e919: function AS_Segment_a1f2060eec6a49a58f0de2fe8116e919(eventobject, sectionNumber, rowNumber) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmOficinaDetails");
        ntf.navigate();
    },
    /** onClick defined for btnPackage **/
    AS_Button_d95804b98df74ede9f12ed8fa2f202d0: function AS_Button_d95804b98df74ede9f12ed8fa2f202d0(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmPaquetes");
        ntf.navigate();
    },
    /** onClick defined for btnOfficinas **/
    AS_Button_e414182aab9f427aacda61ccf734236f: function AS_Button_e414182aab9f427aacda61ccf734236f(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmOficinas");
        ntf.navigate();
    },
    /** onClick defined for btnPreferencias **/
    AS_Button_ic58f0245cf249bfb41fdfb105f57660: function AS_Button_ic58f0245cf249bfb41fdfb105f57660(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmPreferencias");
        ntf.navigate();
    }
});