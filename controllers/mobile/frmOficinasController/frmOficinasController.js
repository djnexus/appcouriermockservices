var serviceName = "CourierAppServices";
var integrationObj = KNYMobileFabric.getIntegrationService(serviceName);

define({ 
  onNavigate: function(){
    this.pauseNavigation();
    kony.application.showLoadingScreen("frmLoad", "Cargando...", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, false);
    this.getData();
    this.view.segOficinasList.onRowClick = this.toDetailsOffice;
  },
  getData: function(){
    var operationName =  "getOfficeInfo";
    var data= {};
    var headers= {};
    integrationObj.invokeOperation(operationName, headers, data, this.operationSuccess, this.operationFailure);
  },
  operationSuccess: function(data){
    this.view.segOficinasList.widgetDataMap = {lblOffice: "name"};
    this.view.segOficinasList.setData(data.responseObject);
    this.resumeNavigation();
    kony.application.dismissLoadingScreen();
  },
  operationFailure: function(data){
    alert("error!");
    this.resumeNavigation();
    kony.application.dismissLoadingScreen();
  },
  toDetailsOffice: function(){
    var navigateToOfficeDetails = new kony.mvc.Navigation("frmOficinaDetails");
    navigateToOfficeDetails.navigate(this.view.segOficinasList.selectedRowItems[0]);
  }
});

