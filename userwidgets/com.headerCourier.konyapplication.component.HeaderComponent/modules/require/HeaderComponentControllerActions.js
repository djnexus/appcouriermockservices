define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnBackButton **/
    AS_Button_jc2f4cec3fd348c59c5bf1cab842dc82: function AS_Button_jc2f4cec3fd348c59c5bf1cab842dc82(eventobject) {
        var self = this;
        return self.goBack.call(this);
    }
});