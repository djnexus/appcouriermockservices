define(function() {

	return {
	onViewCreated: function(){
      this.paquetesIconChange();
    },
      
    paquetesIconChange: function(){
      var currentId = kony.application.getCurrentForm().id;
      if(currentId == "frmPaquetes"){
        this.view.Star.skin = "iconNonFocus";
        this.view.star2.skin = "iconNonFocus";
        this.view.Inbox.skin = "iconFocus";
      }else if(currentId == "frmOficinas"){
        this.view.Inbox.skin = "iconNonFocus";
        this.view.Star.skin = "iconFocus";
      }else if(currentId == "frmPreferencias"){
        this.view.Star.skin = "iconNonFocus";
        this.view.star2.skin = "iconFocus";
      }
    }
	};
});