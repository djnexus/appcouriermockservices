define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnPackage **/
    AS_Button_ab7e9c898d4c48368da4c4f37b808956: function AS_Button_ab7e9c898d4c48368da4c4f37b808956(eventobject) {
        var self = this;
        if (kony.theme.getCurrentTheme() != "default") {
            kony.theme.setCurrentTheme("default", function() {
                self.view.Inbox.skin = "iconFocus";
            }, null);
        } else {
            (function() {
                self.view.Inbox.skin = "iconFocus";
            })();
        }
    },
    /** preShow defined for footerCourierComponent **/
    AS_FlexContainer_g2bfc2cf9c4c46618939b053cdaac89a: function AS_FlexContainer_g2bfc2cf9c4c46618939b053cdaac89a(eventobject) {
        var self = this;
        return self.paquetesIconChange.call(this);
    },
    /** postShow defined for footerCourierComponent **/
    AS_FlexContainer_g371eb664c7b45e2a0cf58910f810b33: function AS_FlexContainer_g371eb664c7b45e2a0cf58910f810b33(eventobject) {
        var self = this;
        return self.paquetesIconChange.call(this);
    }
});